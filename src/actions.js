import { store } from './index'
import newaxios from './newaxios'
import axios from './axios'
import { AsyncStorage } from 'react-native'
import jwt from 'jwt-decode'

export const atualizarLojas = async () => {
  console.log('atualizarLojas')
  const res = await axios.get('loja')
  store.dispatch({ type: 'CLEAR_LOJA' })
  res.data.map(item => {
    store.dispatch({ type: 'ADD_LOJA', item })
  })
}

export const atualizarMotoboys = async () => {
  const res = await axios.get('motoboy')
  store.dispatch({ type: 'CLEAR_MOTOBOY' })
  res.data.map(item => {
    store.dispatch({ type: 'ADD_MOTOBOY', item })
  })
}

export const carregarDadosIniciais = async () => {
  store.dispatch({ type: 'START_LOAD' })
  Promise.all([atualizarLojas(),atualizarMotoboys()]).then(()=>{
    store.dispatch({ type: 'STOP_LOAD' })
  })
}

export const carregarDadosIniciaisERenovar = async () => {
  // os dados iniciais sao carregados
  // mas antes fazemos aautenticacao atravez de token
  // e sempre que essa funcao for chamada o token é atualizado
  try {
    store.dispatch({ type: 'START_LOAD' })
    // pega o token e refrash do banco do aparelho
    const token = await AsyncStorage.getItem('token', false)
    const refreshToken = await AsyncStorage.getItem('refreshtoken', false)
    // se nao existir retorna false para deslogar
    if (!refreshToken) return false
    const decoded = jwt(refreshToken)
    const tempotoken = parseInt((decoded.exp - Date.now() / 1000) / 60)
    // se o token de refresh ja tem menos de 5 segundos desloga
    if (tempotoken < 5) return false
    // se ja se passaram 15 dias atualiza o token de refresh
    if (tempotoken > 1296000) { 
      
      const res = await newaxios.post('/token/refreshtoken', {
        token: refreshToken
      })
     
      await AsyncStorage.setItem('refreshtoken', res.data.token)
    }
    // de toda forma atualiza o token principal e salva
    const res = await newaxios.post('/token/token', {token:refreshToken})
    await AsyncStorage.setItem('token', res.data.token)
    carregarDadosIniciais()
    return true
  } catch (e) {
    store.dispatch({ type: 'STOP_LOAD' })
    if (!e.status) {
      // se der erro de conexao retorna -1
     return -1
    }
    return false
  }
}
