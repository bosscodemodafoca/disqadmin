import { combineReducers } from 'redux'

// aqui temos o comportamento do nosso redux
loading = (state = false, action) => {
  switch (action.type) {
    case 'START_LOAD':
      return true
    case 'STOP_LOAD':
      return false
    default:
      return state
  }
}
motoboy = (state = [], action) => {
  switch (action.type) {
    case 'ADD_MOTOBOY':
      let novo = { ...state }
      novo[action.item._id] = action.item
      return novo
      case 'CLEAR_MOTOBOY':
      return []
    default:
      return state
  }
}
loja = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_LOJA':
      let novo = { ...state }
      novo[action.item._id] = action.item
      return novo
      case 'CLEAR_LOJA':
      return []
    default:
      return state
  }
}

const rootReducer = combineReducers({ motoboy, loja,loading })
export default rootReducer
