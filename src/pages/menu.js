import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  Spinner,
  H1
} from 'native-base'
import { connect } from 'react-redux'
import { AsyncStorage, Alert } from 'react-native'
import jwt from 'jwt-decode'
import axios from '../axios'
import { YellowBox } from 'react-native'
import { carregarDadosIniciaisERenovar,carregarDadosIniciais } from '../actions.js'
import { StackActions } from 'react-navigation'
class menu extends Component {

  // tela com todas as opcoes do app
  constructor (props) {
    super(props)
    YellowBox.ignoreWarnings([
      'Remote debugger is in a background tab which may cause apps to perform slowly',
      'Setting a timer',
      'Unrecognized WebSocket connection'
    ])
  }

  async componentWillMount () {
    // tenta carregar os dados iniciais
    const res = await carregarDadosIniciaisERenovar()
    // caso o retorno seja -1 redireciona para a pagina de sem conexao

    if (res == -1) {
      const NavigationActions = this.props.navigation.actions
      this.props.navigation.reset(
        [NavigationActions.navigate({ routeName: 'semconexao' })],
        0
      )
    } else if (res == false) {
      // caso seja outro erro faz logout
      this.logOut()
    }
  }

  logOut = async () => {
    // o logout limpa todos os dados que estao no aparelho
    // esses dados são os tokens de autenticacao
    // e depois redireciona para a tela de login
    await AsyncStorage.clear()
    const NavigationActions = this.props.navigation.actions
    this.props.navigation.reset(
      [NavigationActions.navigate({ routeName: 'login' })],
      0
    )
  }

  render () {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Menu</Title>
          </Body>
          <Right>
            {this.props.store.loading ? (
              <Spinner />
            ) : (
              <Button transparent onPress={() => carregarDadosIniciais()}>
                <Icon name='refresh' />
              </Button>
            )}
          </Right>
        </Header>
        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('motoboys')
            }}
          >
            <Left>
              <Button>
                <Icon active name='bicycle' />
              </Button>
            </Left>
            <Body>
              <Text>Motoboys</Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('motoboys', { ativo: false })
            }}
          >
            <Left>
              <Button danger>
                <Icon active name='bicycle' />
              </Button>
            </Left>
            <Body>
              <Text>Motoboys Inativos</Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('lojas')
            }}
          >
            <Left>
              <Button style={{ backgroundColor: '#FF9501' }}>
                <Icon active name='briefcase' />
              </Button>
            </Left>
            <Body>
              <Text>Empresas</Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('lojas', { ativo: false })
            }}
          >
            <Left>
              <Button danger>
                <Icon active name='briefcase' />
              </Button>
            </Left>
            <Body>
              <Text>Empresas Inativas</Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('relatorios_recebimento')
            }}
          >
            <Left>
              <Button style={{ backgroundColor: '#227d0d' }}>
                <Icon active name='paper' />
              </Button>
            </Left>
            <Body>
              <Text>Recebimentos</Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('relatorios_pagamento')
            }}
          >
            <Left>
              <Button style={{ backgroundColor: '#227d0d' }}>
                <Icon active name='paper' />
              </Button>
            </Left>
            <Body>
              <Text>Pagamentos</Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('pendentes')
            }}
          >
            <Left>
              <Button style={{ backgroundColor: '#FF9501' }}>
                <Icon active name='warning' />
              </Button>
            </Left>
            <Body>
              <Text>OS Pendentes</Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>
          <Separator bordered />
          <ListItem
            icon
            onPress={() => {
              Alert.alert(
                'Atenção',
                'Tem certeza que deseja fazer logout?',
                [
                  {
                    text: 'Sim',
                    onPress: async () => {
                      // const token = await AsyncStorage.getItem('token')
                      // const res = await axios.post('token/logout', { token })
                      await AsyncStorage.removeItem('token')
                      await AsyncStorage.removeItem('refreshtoken')
                      // this.props.navigation.navigate('login')
                      // this.props.navigation.dispatch(StackActions.popToTop());
                      const NavigationActions = this.props.navigation.actions
                      this.props.navigation.reset(
                        [NavigationActions.navigate({ routeName: 'login' })],
                        0
                      )
                    }
                  },
                  { text: 'Não', onPress: () => {} }
                ],
                { cancelable: false }
              )
            }}
          >
            <Left>
              <Button danger>
                <Icon active name='power' />
              </Button>
            </Left>
            <Body>
              <Text>Logout</Text>
            </Body>
          </ListItem>
          <Separator bordered />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    store: state
  }
}

export default connect(mapStateToProps)(menu)
