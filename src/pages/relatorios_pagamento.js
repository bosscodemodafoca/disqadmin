import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  H3,
  Fab,
  Tab,
  Tabs,
  DatePicker,
  Grid,
  Col,
  Row
} from 'native-base'
import { AsyncStorage } from 'react-native'
import axios from '../axios'
import { connect } from 'react-redux'
import moment from 'moment'

class relatorios extends Component {
  // inicializand as variaveis com o periodo da data atual
  state = {
    motoboy: '',
    res: { valor_total: 0, turnos: [] },
    res2: { valor_total: 0, turnos: [] },
    pagos: [],
    de: moment(),
    ate: moment(),
    load_buscar: false,
    load_pagar: false
  }

  // busca na api tudo a pagar do motoboy selecionado
  getAPagar = async () => {
    try {
      this.setState({ load_buscar: true })
      const res = await axios.post('admin/apagar', {
        motoboy: this.state.motoboy._id
      })
      const data = res.data

      await this.setState({ res: data })
      this.setState({ load_buscar: false })
    } catch (error) {
      this.setState({ load_buscar: false })
    }
  }
  // funcao pra teste
  sleep = ms => {
    return new Promise(resolve => setTimeout(resolve, ms))
  }
  // faz requisicao pra pagar
  pagar = async () => {
    const res = await axios.post('admin/pagar', this.state.res)

    this.getAPagar()
  }

  cancelarPagamento = async _id => {
    const res = await axios.post('admin/cancelarpagamento', { _id })

    alert('Pagamento Cancelado')
    this.getPagos()
  }
  pagarUm = async um => {
    const res = await axios.post('admin/pagar', { turnos: [um] })

    this.getAPagar()
  }

  // pega os pagos na api
  getPagos = async () => {
    try {
      this.setState({ load_buscar: true })
      const res = await axios.post('admin/pagos', this.state)
      const data = res.data

      await this.setState({ pagos: data })
      this.setState({ load_buscar: false })
    } catch (error) {
      this.setState({ load_buscar: false })
    }
  }
  // funcao pra retornar o motoboy selecionado
  setMotoboy = item => {
    const novo = JSON.parse(JSON.stringify(item))
    this.setState({
      motoboy: novo
    })
  }
  // vai pra tela de selecionar motoboy
  gotoMotoboys = () => {
    this.props.navigation.navigate('select_motoboy2', {
      lista: this.props.store.motoboy,
      atual: this.state.motoboy,
      onSelect: this.setMotoboy
    })
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack()
              }}
            >
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>
          <Body>
            <Title>Pagamentos</Title>
          </Body>
          <Right />
        </Header>
        <Tabs>
          <Tab heading='A Pagar'>
            <Content>
              <Separator bordered />
              <ListItem onPress={() => this.gotoMotoboys()}>
                <Body>
                  <Text note>Motoboy</Text>
                  <Text>
                    {this.state.motoboy.nome
                      ? this.state.motoboy.nome
                      : 'Selecione um motoboy'}
                  </Text>
                </Body>
                <Right>
                  <Icon name='arrow-forward' />
                </Right>
              </ListItem>

              <Separator bordered />

              {this.state.load_buscar ? (
                <Button disabled full onPress={() => {}}>
                  <Text>Aguarde</Text>
                </Button>
              ) : (
                <Button
                  full
                  onPress={() => {
                    if (this.state.motoboy._id) this.getAPagar()
                    else alert('Selecione o Motoboy')
                  }}
                >
                  <Text>Buscar</Text>
                </Button>
              )}

              <Separator bordered />
              <ListItem>
                <Body>
                  <Text>
                    Valor Total: R$ {this.state.res.valor_total.toFixed(2)}
                  </Text>
                </Body>
              </ListItem>
              {this.state.res.turnos.map((item, index) => (
                <ListItem
                  key={index}
                  onPress={() => {
                    this.props.navigation.navigate('detalhes', item)
                  }}
                >
                  <View style={{ width: 60 }}>
                    <Button
                      onPress={() => {
                        this.pagarUm(item)
                      }}
                    >
                      <Icon name='checkmark-circle' />
                    </Button>
                  </View>
                  <Body>
                    <Text>
                      Motoboy: {this.props.store.motoboy[item.motoboy].nome}
                    </Text>
                    <Text>
                      Ref:{' '}
                      {moment(item.data)
                        .format('DD/MM/YYYY')
                        .toString()}{' '}
                      #{item.turno + ''}
                    </Text>
                    <Text>
                      Empresa: {this.props.store.loja[item.loja].nome}
                    </Text>
                    <Text>
                      Pedidos: {item.os.length} | Valor: R${' '}
                      {item.valor.toFixed(2) + ''}
                    </Text>
                  </Body>
                </ListItem>
              ))}
              <Separator bordered />
              <Button
                full
                onPress={() => {
                  this.pagar()
                }}
              >
                <Text>Pagar Todos</Text>
              </Button>
              <Separator bordered />
            </Content>
          </Tab>
          {/* ############################################################### */}
          <Tab heading='Pagos'>
            <Content>
              <Grid>
                <Row>
                  <Col>
                    <Text>De</Text>
                    <DatePicker
                      modalTransparent={false}
                      animationType={'fade'}
                      androidMode={'default'}
                      textStyle={{ color: 'green' }}
                      placeHolderText={moment(this.state.de).format(
                        'DD/MM/YYYY'
                      )}
                      onDateChange={e => {
                        this.setState({ de: e })
                      }}
                    />
                  </Col>
                  <Col>
                    <Text>Até</Text>
                    <DatePicker
                      modalTransparent={false}
                      animationType={'fade'}
                      androidMode={'default'}
                      textStyle={{ color: 'green' }}
                      placeHolderText={moment(this.state.ate).format(
                        'DD/MM/YYYY'
                      )}
                      onDateChange={e => {
                        this.setState({ ate: e })
                      }}
                    />
                  </Col>
                </Row>
              </Grid>
              {this.state.load_buscar ? (
                <Button disabled full onPress={() => {}}>
                  <Text>Aguarde</Text>
                </Button>
              ) : (
                <Button
                  full
                  onPress={() => {
                    this.getPagos()
                  }}
                >
                  <Text>Buscar</Text>
                </Button>
              )}
              <Separator bordered />

              {this.state.pagos.map((item, index) => (
                <ListItem
                  key={index}
                  onPress={() => {
                    this.props.navigation.navigate('detalhes', item)
                  }}
                >
                  <View style={{ width: 60 }}>
                    <Button
                      danger
                      onPress={() => {
                        this.cancelarPagamento(item._id)
                      }}
                    >
                      <Icon name='trash' />
                    </Button>
                  </View>
                  <Body>
                    <Text>Motoboy: {item.motoboy.nome}</Text>
                    <Text>
                      Ref:
                      {moment(item.data)
                        .format('DD/MM/YYYY')
                        .toString()}{' '}
                      #{item.turno + ''}
                    </Text>
                    <Text>
                      Pago:{' '}
                      {moment(item.created_at)
                        .format('DD/MM/YYYY HH:mm')
                        .toString()}
                    </Text>
                    <Text>Empresa: {item.loja.nome}</Text>
                    <Text>
                      Pedidos: {item.os.length} | Valor: R${' '}
                      {item.valor.toFixed(2) + ''}
                    </Text>
                  </Body>
                </ListItem>
              ))}
            </Content>
          </Tab>
        </Tabs>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

export default connect(mapStateToProps)(relatorios)
