import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1
} from 'native-base'
import { AsyncStorage, Image, Keyboard } from 'react-native'
import axios from '../axios'
import { carregarDadosIniciais } from '../actions'
import api from '../newaxios';
export default class esqueci extends Component {
  
  constructor(props){
      super(props)
      // ao iniciar a tela ja salva o parametro passado CODIGO
      // o codigo vai ser utilizado pra salvar a nova senha na API
    const codigo = props.navigation.getParam('codigo', false)
      this.state = {
        codigo: codigo,
        password: ''
      }
  }

  recuperar = async () => {
    // envia o codigo e senha pra api
    // mostra o successo ou o erro na requisicao
    if (this.state.password.length < 3) return alert('Senha muito curta!')
    try {
      const res = await axios.post('/senha/novasenha/admin', this.state)
      alert(res.data.msg)

      const NavigationActions = this.props.navigation.actions
      this.props.navigation.reset(
        [NavigationActions.navigate({ routeName: 'login' })],
        0
      )
    } catch (e) {
      
      if (e.response) {
        if (e.response.data.error) alert(e.response.data.error)
      } else alert('Não foi possível conectar ao servidor!')
    }
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Nova Senha</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Image
            style={{ width: '100%', height: 100, resizeMode: 'contain' }}
            source={require('../images/logo_disk.jpg')}
          />
          <Text
            style={{
              padding: 20,
              fontSize: 18,
              fontWeight: 'bold',
              alignSelf: 'center'
            }}
          >
            Nova Senha
          </Text>
          <Form>
            <Item stackedLabel>
              <Label>Digite a Nova Senha</Label>
              <Input
                value={this.state.password}
                onChangeText={e => {
                  this.setState({ password: e })
                }}
                
                autoFocus
                returnKeyType={'next'}
                onSubmitEditing={() => this.recuperar()}
                blurOnSubmit={false}
              />
            </Item>

            <Button
              block
              style={{ marginVertical: 20 }}
              onPress={() => {
                this.recuperar()
              }}
            >
              <Text>Alterar Senha</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    )
  }
}
