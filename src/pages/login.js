import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1
} from 'native-base'
import { AsyncStorage, Image, Keyboard } from 'react-native'
import newaxios from '../newaxios'
import { carregarDadosIniciais } from '../actions'
export default class login extends Component {
  state = {
    user: '',
    password: ''
  }

  login = async () => {
    try {
      // Pra ser feito login é enviado usuario e senha para o servidor
      // no servidor é retornado um token de autenticaçao
      // e outro token usado apenas pra renovar o token

      const res = await newaxios.post('/auth/authenticate/admin', this.state)
      await AsyncStorage.setItem('token', res.data.response.token)
      await AsyncStorage.setItem('refreshtoken', res.data.response.refreshToken)
      carregarDadosIniciais()
      const NavigationActions = this.props.navigation.actions
      this.props.navigation.reset(
        [NavigationActions.navigate({ routeName: 'menu' })],
        0
      )
    } catch (e) {
      if (e.response) {
        if (e.response.data.error) alert(e.response.data.error)
      } else alert('Não foi possível conectar ao servidor!')
    }
  }

  // funcao para melhorar a usabilidade e passar o focu de um input para outro
  _focusNextField (nextField) {
    this.refs[nextField]._root.focus()
  }

  render () {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Autenticação</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Image
            style={{ width: '100%', height: 100, resizeMode: 'contain' }}
            source={require('../images/logo_disk.jpg')}
          />
          <Text
            style={{
              padding: 20,
              fontSize: 18,
              fontWeight: 'bold',
              alignSelf: 'center'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Form>
            <Item stackedLabel>
              <Label>Celular / E-mail</Label>
              <Input
                value={this.state.user}
                onChangeText={e => {
                  this.setState({ user: e })
                }}
                autoFocus
                returnKeyType={'next'}
                onSubmitEditing={() => this._focusNextField('senha')}
                blurOnSubmit={false}
              />
            </Item>
            <Item stackedLabel last>
              <Label>Senha</Label>
              <Input
                ref='senha'
                onSubmitEditing={() => this.login()}
                value={this.state.password}
                onChangeText={e => {
                  this.setState({ password: e })
                }}
              />
            </Item>
            <Button
              block
              style={{ marginVertical: 20 }}
              onPress={() => {
                this.login()
              }}
            >
              <Text>Login</Text>
            </Button>

            <Button
              block
              bordered
              onPress={() => {
                this.props.navigation.navigate('esqueci')
              }}
            >
              <Text>Esqueci a Senha</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    )
  }
}
