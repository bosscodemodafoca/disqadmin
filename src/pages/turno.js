import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  Fab,
  Spinner
} from 'native-base'
import { AsyncStorage } from 'react-native'
import axios from '../axios'
import { connect } from 'react-redux'
class turnos extends Component {
  // formulari padrão
  constructor (props) {
    super(props)
    const item = props.navigation.getParam('item', {})
    const index = props.navigation.getParam('index', -1)

    if (index != -1) {
      var { inicio, garantia, diaria_motoboy, diaria_loja } = item.turnos[index]
    } else {
      var inicio = ''
      var garantia = ''
      var diaria_motoboy = ''
      var diaria_loja = ''
    }
    garantia = garantia + ''
    this.state = { item, index, inicio, garantia, diaria_motoboy, diaria_loja }
  }

  cadastrar = async () => {
    var {
      item,
      index,
      inicio,
      garantia,
      diaria_motoboy,
      diaria_loja
    } = this.state

    diaria_motoboy = diaria_motoboy.replace(",",".")
    diaria_loja = diaria_loja.replace(",",".")


    if (!/\d{2}\:\d{2}/.test(inicio)) {
      alert('Formato de hora inválido! ex(08:00)')
      return
    }

    if (!garantia || !diaria_motoboy || !diaria_loja) {
      alert('Preencha o formulário!')
      return
    }

    this.setState({loading:true})
      try {
        if (index == -1) {
          item.turnos.push({ inicio, garantia, diaria_motoboy, diaria_loja })
        } else {
          item.turnos[index] = { inicio, garantia, diaria_motoboy, diaria_loja }
        }
        const res = await axios.post('/auth/register/loja', item)
        this.props.addLoja(res.data)
        this.props.navigation.goBack()
        this.setState({loading:false})
      } catch (e) {
        this.setState({loading:false})
        if (e.response.data.error) alert(e.response.data.error)
      }
   
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>

          <Body>
            <Title>Turno</Title>
          </Body>
          <Right>
            {this.state.loading ? (
              <Spinner />
            ) : (
              <Button transparent onPress={() => this.cadastrar()}>
              <Text>Salvar</Text>
            </Button>
            )}
          </Right>
        </Header>
        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />
          <Form>
            <Item stackedLabel>
              <Label>Hora de Início</Label>
              <Input
                value={this.state.inicio}
                onChangeText={e => {
                  this.setState({ inicio: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>qtd Garantia</Label>
              <Input
              keyboardType='number-pad'
                value={this.state.garantia}
                onChangeText={e => {
                  this.setState({ garantia: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Diária Motoboy</Label>
              <Input
              keyboardType='decimal-pad'
                value={this.state.diaria_motoboy}
                onChangeText={e => {
                  this.setState({ diaria_motoboy: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Diária Loja</Label>
              <Input
              keyboardType='decimal-pad'
                value={this.state.diaria_loja}
                onChangeText={e => {
                  this.setState({ diaria_loja: e })
                }}
              />
            </Item>
          </Form>
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(turnos)
