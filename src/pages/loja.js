import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  Fab,
  Spinner
} from 'native-base'
// import { NavigationActions } from 'react-navigation';
import { AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import axios from '../axios'

class loja extends Component {
  state = { ativo: true, area1: {} }
  constructor (props) {
    super(props)

    const item = props.navigation.getParam('item', {})
    this.state = { ...item, loading: false }
  }

  cadastrar = async () => {
    // essa tela é o formulario de loja
    // faz a verificacao se é uma nova loja ou uma alteracao
    // e incializa as variaveis
    // ao salvar retorna para a tela anterior
    // e caso haja algum erro da um alerata com os erros
    // retornados do servidor
    this.setState({ loading: true })
    try {
      let nova = false

      if (!this.state._id) nova = true

      let novo = JSON.parse(JSON.stringify(this.state))
      novo.area1.valor = novo.area1.valor.replace(',', '.')

      const res = await axios.post('/auth/register/loja', novo)
      this.props.addLoja(res.data)
      if (nova) {
        const setItem = this.props.navigation.getParam('setItem', () => {})
        setItem({ _id: res.data._id })
      }
      this.props.navigation.goBack()
      this.setState({ loading: false })
    } catch (e) {
      this.setState({ loading: false })
      const data = JSON.parse(JSON.stringify(e.response.data))
      if (data.errors) {
        var msg = ''
        Object.keys(data.errors).map(key => {
          const error = data.errors[key]
          msg += error.message + '\n'
        })
        alert(msg)
      } else {
        alert('Preencha os campos corretamente!')
      }
    }
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack()
              }}
            >
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>
          <Body>
            <Title>Empresa</Title>
          </Body>
          <Right>
            {this.state.loading ? (
              <Spinner />
            ) : (
              <Button transparent onPress={() => this.cadastrar()}>
                <Text>Salvar</Text>
              </Button>
            )}
          </Right>
        </Header>
        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />

          <Form>
            <ListItem icon>
              <Body>
                <Text>Ativo</Text>
              </Body>
              <Right>
                <Switch
                  value={this.state.ativo}
                  onValueChange={e => {
                    this.setState({ ativo: e })
                  }}
                />
              </Right>
            </ListItem>
            <Item stackedLabel>
              <Label>Nome da Empresa</Label>
              <Input
                value={this.state.nome}
                onChangeText={e => {
                  this.setState({ nome: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Telefone Fixo</Label>
              <Input
                keyboardType='phone-pad'
                value={this.state.telefone}
                onChangeText={e => {
                  this.setState({ telefone: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Endereço</Label>
              <Input
                value={this.state.endereco}
                onChangeText={e => {
                  this.setState({ endereco: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>E-mail</Label>
              <Input
                value={this.state.email}
                onChangeText={e => {
                  this.setState({ email: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Nome do Prorietario</Label>
              <Input
                value={this.state.proprietario}
                onChangeText={e => {
                  this.setState({ proprietario: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>WatsApp do Prorietario</Label>
              <Input
                keyboardType='phone-pad'
                value={this.state.celular_proprietario}
                onChangeText={e => {
                  this.setState({ celular_proprietario: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Nome do Responsável</Label>
              <Input
                value={this.state.responsavel}
                onChangeText={e => {
                  this.setState({ responsavel: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>WhatsApp do Responsável</Label>
              <Input
                keyboardType='phone-pad'
                value={this.state.celular}
                onChangeText={e => {
                  this.setState({ celular: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>KM da Área 1</Label>
              <Input
                value={this.state.area1.km + ''}
                onChangeText={e => {
                  let novo = JSON.parse(JSON.stringify(this.state))
                  novo.area1.km = e
                  this.setState(novo)
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Valor da Área 1</Label>
              <Input
                keyboardType='decimal-pad'
                value={this.state.area1.valor + ''}
                onChangeText={e => {
                  let novo = JSON.parse(JSON.stringify(this.state))
                  novo.area1.valor = e
                  this.setState(novo)
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Observações</Label>
              <Input
                value={this.state.observacoes}
                onChangeText={e => {
                  this.setState({ observacoes: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Senha</Label>
              <Input
                value={this.state.password}
                onChangeText={e => {
                  this.setState({ password: e })
                }}
              />
            </Item>
          </Form>
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(loja)
