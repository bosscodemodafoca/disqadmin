import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1
} from 'native-base'
import { AsyncStorage, Image } from 'react-native'
export default class semconexao extends Component {

  // tela de quando esta sem conexao
  // tem a opcao de tentar novamente e de fazer logout
 
    logOut = async () => {
        await AsyncStorage.clear()
        const NavigationActions = this.props.navigation.actions
        this.props.navigation.reset(
          [NavigationActions.navigate({ routeName: 'login' })],
          0
        )
      }
 

  render () {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Sem Conexão</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Image
            style={{ width: '100%', height: 100, resizeMode: 'contain' }}
            source={require('../images/logo_disk.jpg')}
          />
          <Text
            style={{
              padding: 20,
              fontSize: 18,
              fontWeight: 'bold',
              alignSelf: 'center'
            }}
          >
            Sem Conexão com o Servidor
          </Text>
          <Text
            style={{
              padding: 20,
              fontSize: 16,
              
              alignSelf: 'center'
            }}
          >
           Verifique sua conexão com a internet!
          </Text>
          
            <Button
              block
              style={{ marginVertical: 20 }}
              onPress={() => {
                const NavigationActions = this.props.navigation.actions
                this.props.navigation.reset(
                    [NavigationActions.navigate({ routeName: 'menu' })],
                    0
                )
              }}
            >
              <Text>Tentar Novamente</Text>
            </Button>

            <Button
              block
              bordered
              onPress={() => {
                this.logOut()
              }}
            >
              <Text>Logout</Text>
            </Button>
        
        </Content>
      </Container>
    )
  }
}
