import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1,
  Fab,
  Picker,
  Row,
  Col,
  Grid
} from 'native-base'
import colors from '../styles/colors'
import moment from 'moment'
import { connect } from 'react-redux'
import axios from '../axios'
class detalhes extends Component {

  cancelar = async _id => {
    // essa funcao cancela uma os 
    // e volta a para a tela anterior
    const res = await axios.post('os/cancelar', { _id })
    alert('OS Cancelada com Sucesso!')
    this.props.navigation.goBack()
  }
  render () {
    // essa tela da o detalhamento das os em um determinado turno
    
    const item = this.props.navigation.state.params
    item.data_contabilidade_f = moment(item.data_contabilidade).format('DD/MM')
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Detalhes</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <ListItem>
            <Body>
              <Text>
                Turno: #{item.turno} - {item.data_contabilidade_f}
              </Text>
              <Text>
                Qtd de Pedidos: {item.os.length} | Valor: R${' '}
                {item.valor.toFixed(2)}
              </Text>
            </Body>
          </ListItem>
          <Separator />
          {item.os.map((item, index) => {
            item.created_at_f = moment(item.created_at).format(
              'DD/MM/YY | HH:mm'
            )
            item.data_contabilidade_f = moment(item.data_contabilidade).format(
              'DD/MM'
            )
            return (
              <ListItem key={index} noIndent>
                <View style={{ width: 60 }}>
                  <Button
                    danger
                    onPress={() => {
                      this.cancelar(item._id)
                    }}
                  >
                    <Icon name='trash' />
                  </Button>
                </View>
                <View row>
                  <Text style={{ fontWeight: 'bold' }}>{item.loja.nome}</Text>
                  <Text style={{ fontWeight: 'bold' }}>
                    {item.bairro.nome} - {item.area.nome}
                  </Text>
                  <Text>
                    {item.area.km} -{' '}
                    {item.valor != item.area.valor
                      ? item.valor == 0
                        ? 'Garantia'
                        : 'R$ ' +
                          parseFloat(item.valor).toFixed(2) +
                          ' | Garantia '
                      : 'R$ ' + parseFloat(item.valor).toFixed(2)}
                  </Text>

                  <View style={{flexDirection: 'row'}}>
                  <Text>{item.created_at_f}</Text>
                  {item.aceito ? ( <Text style={{fontWeight:'bold' ,color:colors.primary}}> |{moment(item.aceito).format('HH:mm')}</Text>) : null}
                  {item.concluido ? ( <Text style={{fontWeight:'bold' ,color:colors.green}}> |{moment(item.concluido).format('HH:mm')}</Text>) : null}
                 
                  </View>
                  <Text>
                    Status: {item.status} | OS: {item.os}
                  </Text>
                  <Text>
                    Turno: #{item.turno}
                     {/* - {item.data_contabilidade_f} */}
                  </Text>
                 
                  <Text>Boy: {item.motoboy.nome}</Text>
                </View>
              </ListItem>
            )
          })}
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

export default connect(mapStateToProps)(detalhes)
