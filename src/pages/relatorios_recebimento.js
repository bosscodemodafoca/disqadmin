import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  H3,
  Fab,
  Tab,
  Tabs,
  DatePicker,
  Grid,
  Col,
  Row
} from 'native-base'
import { AsyncStorage } from 'react-native'
import axios from '../axios'
import { connect } from 'react-redux'
import moment from 'moment'

class relatorios extends Component {
  state = {
    loja: '',
    res: { valor_total: 0, turnos: [] },
    res2: { valor_total: 0, turnos: [] },
    recebidos: [],
    de: moment(),
    ate: moment(),
    load_buscar: false,
    load_receber: false
  }

  cancelarRecebimento = async _id => {
    const res = await axios.post('admin/cancelarrecebimento', { _id })
    this.sleep(500)

    alert('Recebimento Cancelado')
    this.getRecebidos()
  }
  getAReceber = async () => {
    try {
      this.setState({ load_buscar: true })
      const res = await axios.post('admin/areceber', {
        loja: this.state.loja._id
      })
      const data = res.data
      await this.setState({ res: data })
      this.setState({ load_buscar: false })
    } catch (error) {
      this.setState({ load_buscar: false })
    }
  }

  sleep = ms => {
    return new Promise(resolve => setTimeout(resolve, ms))
  }
  receber = async () => {
    const res = await axios.post('admin/receber', this.state.res)
    // this.sleep(500)
    this.getAReceber()
  }
  receberUm = async um => {
    const res = await axios.post('admin/receber', { turnos: [um] })
    // this.sleep(500)
    this.getAReceber()
  }

  getRecebidos = async () => {
    try {
      this.setState({ load_buscar: true })
      const res = await axios.post('admin/recebidos', this.state)
      const data = res.data

      await this.setState({ recebidos: data })
      this.setState({ load_buscar: false })
    } catch (error) {
      this.setState({ load_buscar: false })
    }
  }

  setLoja = item => {
    const novo = JSON.parse(JSON.stringify(item))
    this.setState({
      loja: novo
    })
  }
  gotoLojas = () => {
    this.props.navigation.navigate('select_loja', {
      lista: this.props.store.loja,
      atual: this.state.loja,
      onSelect: this.setLoja
    })
  }
  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack()
              }}
            >
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>
          <Body>
            <Title>Recebimentos</Title>
          </Body>
          <Right />
        </Header>
        <Tabs>
          <Tab heading='A Receber'>
            <Content>
              <Separator bordered />
              <ListItem onPress={() => this.gotoLojas()}>
                <Body>
                  <Text note>Empresa</Text>
                  <Text>
                    {this.state.loja.nome
                      ? this.state.loja.nome
                      : 'Selecione uma empresa'}
                  </Text>
                </Body>
                <Right>
                  <Icon name='arrow-forward' />
                </Right>
              </ListItem>

              <Separator bordered />
              {this.state.load_buscar ? (
                <Button disabled full onPress={() => {}}>
                  <Text>Aguarde</Text>
                </Button>
              ) : (
                <Button
                  full
                  onPress={() => {
                    if (this.state.loja._id) this.getAReceber()
                    else alert('Selecione a Empresa')
                  }}
                >
                  <Text>Buscar</Text>
                </Button>
              )}

              <Separator bordered />
              <ListItem>
                <Body>
                  <Text>
                    Valor Total: R$ {this.state.res.valor_total.toFixed(2)}
                  </Text>
                </Body>
              </ListItem>
              {this.state.res.turnos.map((item, index) => (
                <ListItem
                  key={index}
                  onPress={() => {
                    this.props.navigation.navigate('detalhes', item)
                  }}
                >
                  <View style={{ width: 60 }}>
                    <Button
                      onPress={() => {
                        this.receberUm(item)
                      }}
                    >
                      <Icon name='checkmark-circle' />
                    </Button>
                  </View>
                  <Body>
                    <Text>
                      Motoboy: {this.props.store.motoboy[item.motoboy].nome}
                    </Text>
                    <Text>
                      Ref:{' '}
                      {moment(item.data)
                        .format('DD/MM/YYYY')
                        .toString()}{' '}
                      #{item.turno + ''}
                    </Text>
                    <Text>
                      Empresa: {this.props.store.loja[item.loja].nome}
                    </Text>
                    <Text>
                      Pedidos: {item.os.length} | Valor: R${' '}
                      {item.valor.toFixed(2) + ''}
                    </Text>
                  </Body>
                </ListItem>
              ))}
              <Separator bordered />
              <Button
                full
                onPress={() => {
                  this.receber()
                }}
              >
                <Text>Receber Todos</Text>
              </Button>
              <Separator bordered />
            </Content>
          </Tab>
          {/* ############################################################### */}
          <Tab heading='Recebidos'>
            <Content>
              <Grid>
                <Row>
                  <Col>
                    <Text>De</Text>
                    <DatePicker
                      modalTransparent={false}
                      animationType={'fade'}
                      androidMode={'default'}
                      textStyle={{ color: 'green' }}
                      placeHolderText={moment(this.state.de).format(
                        'DD/MM/YYYY'
                      )}
                      onDateChange={e => {
                        this.setState({ de: e })
                      }}
                    />
                  </Col>
                  <Col>
                    <Text>Até</Text>
                    <DatePicker
                      modalTransparent={false}
                      animationType={'fade'}
                      androidMode={'default'}
                      textStyle={{ color: 'green' }}
                      placeHolderText={moment(this.state.ate).format(
                        'DD/MM/YYYY'
                      )}
                      onDateChange={e => {
                        this.setState({ ate: e })
                      }}
                    />
                  </Col>
                </Row>
              </Grid>
              {this.state.load_buscar ? (
                <Button disabled full onPress={() => {}}>
                  <Text>Aguarde</Text>
                </Button>
              ) : (
                <Button
                  full
                  onPress={() => {
                    this.getRecebidos()
                  }}
                >
                  <Text>Buscar</Text>
                </Button>
              )}
              <Separator bordered />
              {this.state.recebidos.map((item, index) => (
                <ListItem
                  key={index}
                  onPress={() => {
                    this.props.navigation.navigate('detalhes', item)
                  }}
                >
                  <View style={{ width: 60 }}>
                    <Button
                      danger
                      onPress={() => {
                        this.cancelarRecebimento(item._id)
                      }}
                    >
                      <Icon name='trash' />
                    </Button>
                  </View>
                  <Body>
                    {/* <Text>Motoboy: {item.motoboy.nome}</Text> */}
                    <Text>
                      Ref:{' '}
                      {moment(item.data)
                        .format('DD/MM/YYYY')
                        .toString()}{' '}
                      #{item.turno + ''}
                    </Text>
                    <Text>
                      Recebido:{' '}
                      {moment(item.created_at)
                        .format('DD/MM/YYYY HH:mm')
                        .toString()}
                    </Text>
                    <Text>Empresa: {item.loja.nome}</Text>
                    <Text>
                      Pedidos: {item.os.length} | Valor: R${' '}
                      {item.valor.toFixed(2) + ''}
                    </Text>
                  </Body>
                </ListItem>
              ))}
            </Content>
          </Tab>
        </Tabs>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    store: state
  }
}

export default connect(mapStateToProps)(relatorios)
