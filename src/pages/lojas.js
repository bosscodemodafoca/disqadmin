import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1,
  Fab
} from 'native-base'
import { AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import axios from '../axios'
import {atualizarLojas} from '../actions'
import {Alert} from 'react-native'
class lojas extends Component {
  constructor (props) {
    super(props)
    const ativo = props.navigation.getParam('ativo', true)
    this.state = { ativo }
  }

  // Essa tela exibe a listagem das lojas
  // e recebido como paramentro se é para ser exibido
  // as lojas ativas ou inativas

  render () {

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Empresas</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />

          {Object.keys(this.props.store.loja).map((key, index) => {
            const item = this.props.store.loja[key]
            if (this.state.ativo == item.ativo) {
            return (
              <ListItem
                icon
                key={index}
                onPress={() => {
                  this.props.navigation.navigate('setores', { item })
                }}
              >
                <Left>
                    <Button
                      danger={!item.ativo}
                      transparent={!!item.ativo}
                      onPress={() => {
                        if (!item.ativo) {
                          Alert.alert(
                            'Atenção',
                            'Você deseja excluir permanentemente essa Empresa?',
                            [
                              {
                                text: 'Sim',
                                onPress: async () => {
                                  await axios.post('/loja/deletar', {
                                    _id: item._id
                                  })
                                 atualizarLojas()
                                }
                              },
                              { text: 'Não', onPress: () => {} }
                            ],
                            { cancelable: false }
                          )
                        }
                      }}
                    >
                      <Icon active name={item.ativo ? 'checkmark' : 'trash'} />
                    </Button>
                  </Left>
                <Body>
                  <Text>{item.nome}</Text>
                  {item.observacoes != ''
                    ? <Text note>obs.{item.observacoes}</Text>
                    : <View/> }

                </Body>
                <Right>
                  <Icon name='create' />
                </Right>
              </ListItem>
            )
            }
          })}

          <Separator bordered />
        </Content>
        {!this.state.ativo ? null : (
        <Fab
          style={{ backgroundColor: '#5067FF' }}
          position='bottomRight'
          onPress={() => this.props.navigation.navigate('setores')}
        >
          <Icon name='add' />
        </Fab>
        )}
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(lojas)
