import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1
} from 'native-base'
import { AsyncStorage,Image,Keyboard } from 'react-native'
import axios from '../axios'
import {carregarDadosIniciais} from '../actions'
export default class esqueci extends Component {
  state = {
    user: '',
    password: ''
  }

  enviarcodigo = async () => {
    // essa tela é responsável por enviar o usuario para recuperacao de senha
    // depois disso ele da o alerta com a mensagem de sucesso ou erro
    // retornada do servidor
    if (!this.state.user)
      return alert("Digite o Celular ou E-mail!")
    try {
      const res = await axios.post('/senha/esqueci/admin', this.state)
      alert(res.data.msg)
      this.props.navigation.navigate("recuperar")
    } catch (e) {
  
     if (e.response){
      if (e.response.data.error) alert(e.response.data.error)
    }
      else alert("Não foi possível conectar ao servidor!")

    }
  }



  render () {
    return (
      <Container>
        <Header>
        <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Recuperação</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
        <Image        
        style={{ width: '100%',height:100, resizeMode: 'contain'}}
          source={require('../images/logo_disk.jpg')}
        />
        <Text
            style={{
              padding: 20,
              fontSize: 18,
              fontWeight: 'bold',
              alignSelf:"center"
            }}
          >
            Recuperação de Senha
          </Text>
          <Form>
            <Item stackedLabel>
              <Label>Celular / E-mail</Label>
              <Input
                value={this.state.user}
                onChangeText={e => {
                  this.setState({ user: e })
                }}
                autoFocus={true}
                returnKeyType = { "next" }
                onSubmitEditing={() => this.enviarcodigo()}
                blurOnSubmit={false}
              />
            </Item>
            
            <Button
              block
              style={{ marginVertical: 20 }}
              onPress={() => {
                this.enviarcodigo()
              }}
            >
              <Text>Avançar</Text>
            </Button>

            <Button block bordered onPress={()=>{ this.props.navigation.navigate('recuperar') }}>
              <Text>Já tenho um código de Recuperação</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    )
  }
}
