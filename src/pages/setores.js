import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1,
  Fab,
  Card,
  CardItem,
  Grid,
  Row,
  Col
} from 'native-base'
import colors from '../styles/colors'
import { AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import axios from '../axios'

class setores extends Component {
  state = { item: {} }
// essa tela é onde e exibido os detalhes da empresa
// nela temos a opcao de adicionar bairro,area,turno e editar empresa
  constructor (props) {
    super(props)
    const item = props.navigation.getParam('item', false)
    // faz uma verificacao se existe uma empresa com id valida
    // caso nao volta p tela anterior
    // isso acontece pq o historico da navegacao é
    // empresas>setores>empresa
    // e caso o usuario volte de empresa, ele nao vai ter uma empresa cadastrada
    // entao deve voltar pra tela de listagem de empresas
    props.navigation.addListener('didFocus', () => {
      if (!this.state.item._id) props.navigation.goBack()
    })
    if (item) {
      // item.garantia = item.garantia.toString()
      this.state = { item }
    } else {
      props.navigation.navigate('loja', {
        item: {
          ativo: true,
          area1: { nome: 'Area 1', km: '', valor: '' }
        },
        setItem: this.setItem
      })
    }
  }
  setItem = item => {
    this.setState({ item: item })
  }
// apaga um turno
  dropTurno = async index => {
    try {
      let novo = JSON.parse(
        JSON.stringify(this.props.store.loja[this.state.item._id])
      )
      novo.turnos.splice(index, 1)
      const res = await axios.post('/auth/register/loja', novo)
      this.props.addLoja(res.data)
    } catch (e) {
      if (e.response.data.error) alert(e.response.data.error)
    }
  }
  // apaga um setor nome atualmente chamado de area
  dropSetor = async index => {
    try {
      let novo = JSON.parse(
        JSON.stringify(this.props.store.loja[this.state.item._id])
      )
      novo.areas.splice(index, 1)

      const res = await axios.post('/auth/register/loja', novo)
      this.props.addLoja(res.data)
    } catch (e) {
      if (e.response.data.error) alert(e.response.data.error)
    }
  }
  // apaga um bairro
  dropBairro = async (index_area, index_bairro) => {
    try {
      let novo = JSON.parse(
        JSON.stringify(this.props.store.loja[this.state.item._id])
      )

      if (index_area == -1) novo.area1.bairro.splice(index_bairro, 1)
      else novo.areas[index_area].bairro.splice(index_bairro, 1)
      const res = await axios.post('/auth/register/loja', novo)

      this.props.addLoja(res.data)
    } catch (e) {
      if (e.response.data.error) alert(e.response.data.error)
    }
  }
  render () {
    if (!this.state.item._id) {
      return <View />
    }
    console.log(this.props.store.loja)
    const item = this.props.store.loja[this.state.item._id]
    var qtdbairros = 0
    item.area1.bairro.map(() => qtdbairros++)
    item.areas.map(area => area.bairro.map(() => qtdbairros++))
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Empresa</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          {/* <Separator bordered /> */}
          <Card>
            <CardItem>
              <Body>
                <Text note>Empresa</Text>
                <Text>
                  {item.nome} - {item.telefone}
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text note>Responsável</Text>
                <Text>
                  {item.responsavel} - {item.celular}
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text note>Proprietário</Text>
                <Text>
                  {item.proprietario} - {item.celular_proprietario}
                </Text>
              </Body>
            </CardItem>

            {item.observacoes ? (
              <CardItem>
                <Body>
                  <Text note>Obs.</Text>
                  <Text>{item.observacoes}</Text>
                </Body>
              </CardItem>
            ) : null}
          </Card>
          {item.motoboys.length > 0 ? null : (
            <ListItem noIndent style={{ backgroundColor: colors.bgdanger }}>
              <Left>
                <Text>Você precisa selecionar os Motoboys</Text>
              </Left>
            </ListItem>
          )}
          {item.turnos.length > 0 ? null : (
            <ListItem noIndent style={{ backgroundColor: colors.bgdanger }}>
              <Left>
                <Text>Você adicionar os Turnos</Text>
              </Left>
            </ListItem>
          )}
          {qtdbairros > 0 ? null : (
            <ListItem noIndent style={{ backgroundColor: colors.bgdanger }}>
              <Left>
                <Text>Você precisa adicionar os Bairros</Text>
              </Left>
            </ListItem>
          )}
          <Separator bordered />
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('loja', { item })
            }}
          >
            <Left>
              <Button>
                <Icon active name='create' />
              </Button>
            </Left>
            <Body>
              <Text>Editar Dados da Empresa</Text>
            </Body>
            <Right />
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('setor', { item })
            }}
          >
            <Left>
              <Button style={{ backgroundColor: '#FF9501' }}>
                <Icon active name='locate' />
              </Button>
            </Left>
            <Body>
              <Text>Adicionar Area</Text>
            </Body>
            <Right />
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('bairro', { item })
            }}
          >
            <Left>
              <Button style={{ backgroundColor: '#f81f21' }}>
                <Icon active name='map' />
              </Button>
            </Left>
            <Body>
              <Text>Adicionar Bairro</Text>
            </Body>
            <Right />
          </ListItem>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('turno', { item })
            }}
          >
            <Left>
              <Button style={{ backgroundColor: '#3bd148' }}>
                <Icon active name='time' />
              </Button>
            </Left>
            <Body>
              <Text>Adicionar Turno</Text>
            </Body>
            <Right />
          </ListItem>

          <Separator bordered />
          <ListItem
            onPress={() => {
              this.props.navigation.navigate('select_motoboy', { item })
            }}
          >
            <Body>
              <Text>Motoboys</Text>
              {item.motoboys.length > 0 ? (
                item.motoboys.map((motoboy_id, index) => {
                  const motoboy = this.props.store.motoboy[motoboy_id]
                  if (motoboy) {
                    if (motoboy.ativo) {
                      return (
                        <Text key={index} note>
                          {motoboy.nome} - {motoboy.whatsapp}{' '}
                          <Icon
                            style={{ fontSize: 12, color: 'gray' }}
                            name='logo-whatsapp'
                          />
                        </Text>
                      )
                    }
                  }
                })
              ) : (
                <Text note>Selecione os Motoboys</Text>
              )}
            </Body>
            <Right>
              <Icon name='create' />
            </Right>
          </ListItem>

          <Separator bordered>
            <Text>Áreas</Text>
          </Separator>
          <ListItem
            icon
            onPress={() => {
              this.props.navigation.navigate('loja', { item })
            }}
          >
            <Left>
              <Button transparent disabled>
                <Icon name='ios-remove-circle' />
              </Button>
            </Left>
            <Body>
              <Text>
                Área 1 - {item.area1.km} / R${' '}
                {parseFloat(item.area1.valor).toFixed(2)}
              </Text>
            </Body>
            <Right>
              <Icon name='create' />
            </Right>
          </ListItem>
          {item.areas.map((setor, index) => (
            <ListItem
              icon
              key={index}
              onPress={() => {
                this.props.navigation.navigate('setor', { item, index })
              }}
            >
              <Left>
                <Button
                  danger
                  transparent
                  onPress={() => this.dropSetor(index)}
                >
                  <Icon name='ios-remove-circle' />
                </Button>
              </Left>
              <Body>
                <Text>
                  {setor.nome} - {setor.km}Km /R${' '}
                  {parseFloat(setor.valor).toFixed(2)}
                </Text>
              </Body>
              <Right>
                <Icon name='create' />
              </Right>
            </ListItem>
          ))}
          <Separator bordered>
            <Text>Bairros</Text>
          </Separator>
          {item.area1.bairro.map((bairro, b_i) => {
            return (
              <ListItem
                key={b_i}
                onPress={() => {
                  this.props.navigation.navigate('bairro', {
                    item,
                    index_area: -2,
                    index_bairro: b_i
                  })
                }}
              >
                <View>
                  <Button
                    danger
                    transparent
                    onPress={() => this.dropBairro(-1, b_i)}
                  >
                    <Icon name='ios-remove-circle' />
                  </Button>
                </View>
                <Body>
                  <Text>{bairro.nome} - Área 1</Text>
                  <View />
                </Body>
                <Right>
                  <Icon name='create' />
                </Right>
              </ListItem>
            )
          })}
          {item.areas.map((setor, index) =>
            setor.bairro.map((bairro, b_i) => {
              return (
                <ListItem
                  key={index + '' + b_i}
                  onPress={() => {
                    this.props.navigation.navigate('bairro', {
                      item,
                      index_area: index,
                      index_bairro: b_i
                    })
                  }}
                >
                  <View>
                    <Button
                      danger
                      transparent
                      onPress={() => this.dropBairro(index, b_i)}
                    >
                      <Icon name='ios-remove-circle' />
                    </Button>
                  </View>
                  <Body>
                    <Text>
                      {bairro.nome} - {setor.nome}
                    </Text>
                    {bairro.rua == '' ? (
                      <View />
                    ) : (
                      bairro.rua.split(',').map((rua, index) => {
                        return (
                          <Text note key={index}>
                            {rua}
                          </Text>
                        )
                      })
                    )}
                  </Body>
                  <Right>
                    <Icon name='create' />
                  </Right>
                </ListItem>
              )
            })
          )}
          <Separator bordered>
            <Text>Turnos</Text>
          </Separator>
          {item.turnos.map((turno, index) => (
            <ListItem
              icon
              key={index}
              onPress={() => {
                this.props.navigation.navigate('turno', { item, index })
              }}
            >
              <Left>
                <Button
                  danger
                  transparent
                  onPress={() => this.dropTurno(index)}
                >
                  <Icon name='ios-remove-circle' />
                </Button>
              </Left>
              <Body>
                <Text>
                  {turno.inicio} - Garantia: {turno.garantia}
                </Text>
              </Body>
              <Right>
                <Icon name='create' />
              </Right>
            </ListItem>
          ))}
          <Separator bordered />
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}
const mapDispatchToProps = dispatch => {
  return {
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(setores)
