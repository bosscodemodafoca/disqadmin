import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  Fab,
  Spinner
} from 'native-base'
import { AsyncStorage } from 'react-native'
import axios from '../axios'
import { connect } from 'react-redux'
class motoboy extends Component {
  state = { ativo: true }
  // tela de formulario simples
  // vizualiza e salva
  constructor (props) {
    super(props)

    const item = props.navigation.getParam('item', false)
    if (item) {
      this.state = {...item,loading:false}
    }
  }
  cadastrar = async () => {
    this.setState({loading:true})
    try {
      const res = await axios.post('/auth/register/motoboy', this.state)

      this.props.addMotoboys(res.data)
      this.props.navigation.goBack()
      this.setState({loading:false})
    } catch (e) {
      this.setState({loading:false})
      const data = JSON.parse(JSON.stringify(e.response.data))

      if (data.errors) {
        var msg = ''
        Object.keys(data.errors).map(key => {
          const error = data.errors[key]
          msg += error.message + '\n'
        })
        alert(msg)
        
      } else {
       
        alert('Preencha os campos corretamente!')
      }
    }
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>

          <Body>
            <Title>Motoboys</Title>
          </Body>
          <Right>
            {this.state.loading ? (
              <Spinner />
            ) : (
              <Button transparent onPress={() => this.cadastrar()}>
              <Text>Salvar</Text>
            </Button>
            )}
          </Right>
        </Header>

        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />
          <Form>
            <ListItem icon>
              <Body>
                <Text>Ativo</Text>
              </Body>
              <Right>
                <Switch
                  value={this.state.ativo}
                  onValueChange={e => {
                    this.setState({ ativo: e })
                  }}
                />
              </Right>
            </ListItem>
            <Item stackedLabel>
              <Label>Nome</Label>
              <Input
                value={this.state.nome}
                onChangeText={e => {
                  this.setState({ nome: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Whatsapp</Label>
              <Input
                keyboardType='phone-pad'
                value={this.state.whatsapp}
                onChangeText={e => {
                  this.setState({ whatsapp: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Celular</Label>
              <Input
                keyboardType='phone-pad'
                value={this.state.celular}
                onChangeText={e => {
                  this.setState({ celular: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>CPF</Label>
              <Input
                keyboardType='numeric'
                value={this.state.cpf}
                onChangeText={e => {
                  this.setState({ cpf: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Placa da Moto</Label>
              <Input
                value={this.state.placa}
                onChangeText={e => {
                  this.setState({ placa: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>E-mail</Label>
              <Input
                keyboardType='email-address'
                value={this.state.email}
                onChangeText={e => {
                  this.setState({ email: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Senha</Label>
              <Input
                value={this.state.password}
                onChangeText={e => {
                  this.setState({ password: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Observaçao</Label>
              <Input
                value={this.state.observacoes}
                onChangeText={e => {
                  this.setState({ observacoes: e })
                }}
              />
            </Item>
          </Form>
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addMotoboys: item => dispatch({ type: 'ADD_MOTOBOY', item })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(motoboy)
