import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail
} from 'native-base'
import { styles } from '../styles'
class lojas extends Component {
  render () {
    // tela select padrao
    // onde recebo a lista
    // o item que esta selecionado atualmente
    // e a funcao que deve ser executada ao clicar em um item 
    // no caso selecionar

    const { onSelect, atual, lista } = this.props.navigation.state.params
    const { navigation } = this.props

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Selecione</Title>
          </Body>
          <Right />
        </Header>
        <Content>
        
          {Object.keys(lista).map(key => {
            const item = lista[key]
            if (item.ativo)
            return (


            <ListItem
              icon
              key={item._id}
              onPress={() => {
                onSelect(item)
                navigation.goBack()
              }}
            >
              <Left>
                <Button transparent light={item._id != atual._id}>
                  <Icon name='ios-checkmark-circle' />
                </Button>
              </Left>
              <Body>
                <Text>{item.nome}</Text>
              </Body>

            </ListItem>
          )})}

        </Content>
      </Container>
    )
  }
}
export default lojas
