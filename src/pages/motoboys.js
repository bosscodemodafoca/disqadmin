import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1,
  Fab
} from 'native-base'
import {Alert} from 'react-native'
import { connect } from 'react-redux'
import axios from '../axios'
import {atualizarMotoboys} from '../actions'
class motoboys extends Component {
  // listagem de motoboys com o parametro 
  // se para ser exibido os ativos ou inativos
  constructor (props) {
    super(props)
    const ativo = props.navigation.getParam('ativo', true)
    this.state = { ativo }
  }


  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>

          <Body>
            <Title>Motoboys</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />
          {Object.keys(this.props.store.motoboy).map((key, index) => {
            const item = this.props.store.motoboy[key]
            if (this.state.ativo == item.ativo) {
              return (
                <ListItem
                  icon
                  key={index}
                  onPress={() => {
                    this.props.navigation.navigate('motoboy', { item })
                  }}
                >
                  <Left>
                    <Button
                      danger={!item.ativo}
                      transparent={!!item.ativo}
                      onPress={() => {
                        if (!item.ativo) {
                          Alert.alert(
                            'Atenção',
                            'Você deseja excluir permanentemente esse Motoboy?',
                            [
                              {
                                text: 'Sim',
                                onPress: async () => {
                                  await axios.post('/motoboy/deletar', {
                                    _id: item._id
                                  })
                                 atualizarMotoboys()
                                }
                              },
                              { text: 'Não', onPress: () => {} }
                            ],
                            { cancelable: false }
                          )
                        }
                      }}
                    >
                      <Icon active name={item.ativo ? 'checkmark' : 'trash'} />
                    </Button>
                  </Left>
                  <Body>
                    <Text>{item.nome}</Text>
                    <Text note>{item.celular}</Text>
                  </Body>
                  <Right>
                    <Icon name='create' />
                  </Right>
                </ListItem>
              )
            }
          })}
          <Separator bordered />
        </Content>
        {!this.state.ativo ? null : (
          <Fab
            style={{ backgroundColor: '#5067FF' }}
            position='bottomRight'
            onPress={() => this.props.navigation.navigate('motoboy')}
          >
            <Icon name='add' />
          </Fab>
        )}
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addMotoboys: item => dispatch({ type: 'ADD_MOTOBOY', item }),
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(motoboys)
