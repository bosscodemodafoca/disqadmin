import adicionais from './adicionais';
import atributos from './atributos';
import lojas from './lojas';
import produto from './produto';
import produtos from './produtos';
import pedido from './pedido';
import cadastro from './cadastro';



export { adicionais,atributos,lojas,produto,produtos,pedido,cadastro };
