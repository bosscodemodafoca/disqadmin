import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1
} from 'native-base'
import { AsyncStorage,Image,Keyboard } from 'react-native'
import axios from '../axios'
import {carregarDadosIniciais} from '../actions'
export default class esqueci extends Component {
  state = {
    codigo: ''
   
  }

  recuperar = async () => {
    // aqui é enviado o codigo de 6 digitos pro servidor
    // e e recebido a mensagem se deu certo ou erro
    // se der certo redireciona pra tela de nova senha
    // se der erro mostra a mensage
    if (this.state.codigo.length != 6)
      return alert("Digite o Código de verificação! (6 Digitos)")
    try {
      const res = await axios.post('/senha/verificar/admin', this.state)
    //   alert(res.data.msg)
      this.props.navigation.navigate("novasenha",{codigo:this.state.codigo})
    } catch (e) {
      
     if (e.response){
      if (e.response.data.error) alert(e.response.data.error)
    }
      else alert("Não foi possível conectar ao servidor!")

    }
  }

  

  render () {
    return (
      <Container>
        <Header>
        <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Código</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
        <Image        
        style={{ width: '100%',height:100, resizeMode: 'contain'}}
          source={require('../images/logo_disk.jpg')}
        />
        <Text
            style={{
              padding: 20,
              fontSize: 18,
              fontWeight: 'bold',
              alignSelf:"center"
            }}
          >
            Código de Verificação
          </Text>
          <Form>
            <Item stackedLabel>
              <Label>Digite o Código</Label>
              <Input
                value={this.state.codigo}
                onChangeText={e => {
                  this.setState({ codigo: e })
                }}
                keyboardType='numeric'
                autoFocus={true}
                returnKeyType = { "next" }
                onSubmitEditing={() => this.recuperar()}
                blurOnSubmit={false}
              />
            </Item>
            
            <Button
              block
              style={{ marginVertical: 20 }}
              onPress={() => {
                this.recuperar()
              }}
            >
              <Text>Avançar</Text>
            </Button>

            
          </Form>
        </Content>
      </Container>
    )
  }
}
