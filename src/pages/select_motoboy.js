import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  View,
  Thumbnail,
  H1,
  Fab,
  Radio,
  Spinner
} from 'native-base'
import { AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import axios from '../axios'

class selectMotoboys extends Component {
  // tela onde e selecionado os motoboys de uma determinada empresa
  state = { item: {},loading:false }

  constructor (props) {
    super(props)
    const item = JSON.parse(
      JSON.stringify(props.navigation.getParam('item', false))
    )
    this.state = { item }
   
  }

  clickItem = async (item) => {
    const novo = JSON.parse(JSON.stringify(this.state.item))
    const index = novo.motoboys.indexOf(item._id)

    if (index != -1) {
      novo.motoboys.splice(index, 1)
    } else {
      novo.motoboys.push(item._id)
    }
    await this.setState({ item: novo })
  }

  cadastrar = async () => {
    // salva no banco a empresa completa
    const novo = JSON.parse(JSON.stringify(this.state.item))
    this.setState({loading:true})
    try {
      this.state.item.motoboys.map(async (item, index) => {
        const x = this.props.store.motoboy[item]

        if (!x) {
          novo.motoboys.splice(index, 1)
        }
      })

      const res = await axios.post('/auth/register/loja', novo)
   
      this.props.addLoja(res.data)
      this.props.navigation.goBack()
      this.setState({loading:false})
    } catch (e) {
      this.setState({loading:false})
      if (e.response.data.error) alert(e.response.data.error)
    }
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>
          <Body>
            <Title>Selecione</Title>
          </Body>
          <Right>
            {this.state.loading ? (
              <Spinner />
            ) : (
              <Button transparent onPress={() => this.cadastrar()}>
              <Text>Salvar</Text>
            </Button>
            )}
          </Right>
        </Header>
        <Content>
          <Separator bordered />
          {Object.keys(this.props.store.motoboy).map((key, index) => {
            const item = this.props.store.motoboy[key]
            if (!!item.ativo)
            return (
              <ListItem
                key={index}
                onPress={() => {
                  this.clickItem(item)
                }}
              >
                <Left>
                  <Text>{item.nome}</Text>
                </Left>
                <Right>
                  <Radio
                    selected={this.state.item.motoboys.includes(item._id)}
                  />
                </Right>
              </ListItem>
            )
          })}
          <Separator bordered />
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(selectMotoboys)
