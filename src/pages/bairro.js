import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  Fab,
  Picker,
  Spinner
} from 'native-base'
import { AsyncStorage } from 'react-native'
import axios from '../axios'
import { connect } from 'react-redux'
class bairro extends Component {
  constructor (props) {
    super(props)

    // Na tela de cadastro de bairro é recebido como parametro
    // o ITEM que é referente a LOJA completa
    // o indice da area e o indice do bairro caso seja uma alteraçao
    // caso não o valor padrão é -1

    const item = props.navigation.getParam('item', {})
    const index_area = props.navigation.getParam('index_area', -1)
    const index_bairro = props.navigation.getParam('index_bairro', -1)
    var area_id = -1

    var bairro_id = -1

    // aqui começamos as verificações
    // caso nao tenhamos indice indica que é totalmente novo
    // entao as variazveis sao inicializadas
    if (index_area == -1 || index_bairro == -1) {
      var nome = ''
      var rua = ''
      var area = '1'
    } else if (index_area == -2) {
      // se o indice da area for -2 indica que é referete a area 1
      // ou seja a area padrão da LOJA
      var { _id, nome, rua } = item.area1.bairro[index_bairro]
      bairro_id = _id
      var area = '1'
    } else {
      // caso contrario é apenas uma alteraçao comun
      // nas demais areas
      var { _id, nome, rua } = item.areas[index_area].bairro[index_bairro]
      var area = item.areas[index_area]._id
      area_id = area
      bairro_id = _id
    }
    this.state = {
      item,
      index_area,
      index_bairro,
      nome,
      rua,
      area,
      area_id,
      bairro_id,
      loading:false
    }
  }

  cadastrar = async () => {
    const {
      item,
      index_area,
      index_bairro,
      nome,
      rua,
      area_id,
      bairro_id
    } = this.state

    // ao cadastrar verifica-se se o nome esta preenchido
    if (!nome) {
      alert('Preencha o Nome do Bairro!')
      return
    }
this.setState({loading:true})
    // isso vai acontecer em caso de alteraçao 
    // verificando se o bairro já possui id
    if (bairro_id != -1) {
      // se  a area nao tiver id que dizer ue ela é a area 1
      //entao é removido o bairro da area 1
      if (area_id == -1) {
        item.area1.bairro.map((bairro, index) => {
          if (bairro._id == bairro_id) {
            item.area1.bairro.splice(index, 1)
          }
        })
      } else {
        // caso a area possua id removemos o bairro daquela area
        item.areas.map((area, i) => {
          if (area._id == area_id) {
            area.bairro.map((bairro, index) => {
              if (bairro._id == bairro_id) {
                item.areas[i].bairro.splice(index, 1)
              }
            })
          }
        })
      }
    }

    try {
      // agora adicionamos o bairro novamente na sua respectiva area
      if (this.state.area == '1') item.area1.bairro.push({ nome, rua })
      else {
        item.areas.map((item, index) => {
          if (item._id == this.state.area) item.bairro.push({ nome, rua })
        })
      }
      // mandamos os dados pro servido e com o retorno atualizamos nossa lista
      // e voltamos a tela
      const res = await axios.post('/auth/register/loja', item)
      this.props.addLoja(res.data)
      this.props.navigation.goBack()
      this.setState({loading:false})
    } catch (e) {
      this.setState({loading:false})
      if (e.response.data.error) alert(e.response.data.error)
    }
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>
          <Body>
            <Title>Bairro</Title>
          </Body>
          <Right>
            {this.state.loading ? (
              <Spinner />
            ) : (
              <Button transparent onPress={() => this.cadastrar()}>
              <Text>Salvar</Text>
            </Button>
            )}
          </Right>
        </Header>

        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />
          <Form>
            <Item stackedLabel>
              <Label>Nome do Bairro</Label>
              <Input
                value={this.state.nome}
                onChangeText={e => {
                  this.setState({ nome: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Ruas</Label>
              <Input
                value={this.state.rua}
                onChangeText={e => {
                  this.setState({ rua: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Área</Label>
              <Picker
                note
                mode='dropdown'
                style={{ width: 250 }}
                selectedValue={this.state.area}
                onValueChange={e => {
                  this.setState({ area: e })
                }}
              >
                <Picker.Item label='Área 1' value='1' />
                {this.state.item.areas.map((area, index) => {
                  return (
                    <Picker.Item
                      key={index}
                      label={area.nome}
                      value={area._id}
                    />
                  )
                })}
              </Picker>
            </Item>
          </Form>
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(bairro)
