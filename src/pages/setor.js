import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  Fab,
  Spinner
} from 'native-base'
import { AsyncStorage } from 'react-native'
import axios from '../axios'
import { connect } from 'react-redux'
class setor extends Component {
  constructor (props) {
    // formulario padrao
    super(props)
    const item = props.navigation.getParam('item', {})
    const index = props.navigation.getParam('index', -1)


    if (index != -1) {
      var { nome, km, valor } = item.areas[index]
    } else {
      var nome = '', km = '', valor = ''
    }

    this.state = { item, index, nome, km, valor,loading:false }
  }

  cadastrar = async () => {
    var { item, index, nome, km, valor } = this.state
    if (!nome || !km || !valor) {
      alert('Preencha o formulário!')
      return
    }
    try {
      this.setState({loading:true})
      valor = valor.replace(",",".")
      if (index == -1) {
        item.areas.push({ nome, km, valor })
      } else {
        item.areas[index] = { nome, km, valor }
      }
      const res = await axios.post('/auth/register/loja', item)
      this.props.addLoja(res.data)
      this.props.navigation.goBack()
      this.setState({loading:false})
    } catch (e) {
      this.setState({loading:false})
      if (e.response.data.error) alert(e.response.data.error)
    }
  }

  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
              <Text>Cancelar</Text>
            </Button>
          </Left>

          <Body>
            <Title>Nome da Área</Title>
          </Body>

          <Right>
            {this.state.loading ? (
              <Spinner />
            ) : (
              <Button transparent onPress={() => this.cadastrar()}>
              <Text>Salvar</Text>
            </Button>
            )}
          </Right>
        </Header>

        <Content>
          <Text
            style={{
              padding: 30,
              fontSize: 24,
              fontWeight: 'bold'
            }}
          >
            DisqMotoboy(Admin)
          </Text>
          <Separator bordered />
          <Form>
            <Item stackedLabel>
              <Label>Nome da Área</Label>
              <Input
                value={this.state.nome}
                onChangeText={e => {
                  this.setState({ nome: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Km</Label>
              <Input
                value={this.state.km}
                onChangeText={e => {
                  this.setState({ km: e })
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Valor</Label>
              <Input
              keyboardType='decimal-pad'
                value={this.state.valor}
                onChangeText={e => {
                  this.setState({ valor: e })
                }}
              />
            </Item>
          </Form>
        </Content>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    store: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addLoja: item => dispatch({ type: 'ADD_LOJA', item })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(setor)
