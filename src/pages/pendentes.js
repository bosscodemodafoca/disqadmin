import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Label,
  Input,
  Title,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Text,
  Separator,
  Button,
  Icon,
  Switch,
  View,
  Thumbnail,
  H1,
  H3,
  Fab,
  Tab,
  Tabs,
  DatePicker,
  Grid,
  Col,
  Row
} from 'native-base'
import { AsyncStorage } from 'react-native'
import axios from '../axios'
import { connect } from 'react-redux'
import moment from 'moment'

class relatorios extends Component {
  // nessa tela fazemos a listagem de todas as os pendentes
  state = {
    loja: '',
    res: [],
    load_buscar: false
  }
  // funcao que cancela uma os e atualiza a lista
  cancelar = async _id => {
    const res = await axios.post('os/cancelar', { _id })
    alert('OS Cancelada com Sucesso!')
    this.getPendentes()
  }
  // funcao qe cancela todas as os que estao na tela
  // e no final de tudo atualiza a lista
  cancelarTodos = async _id => {
    const result = this.state.res.map(async (item, i) => {
      await axios.post('os/cancelar', { _id: item._id })
    })
    Promise.all(result).then(() => {
      alert('OS Cancelada com Sucesso!')
      this.getPendentes()
    })
  }
  // atualiza a lista

  getPendentes = async () => {

    try {
      this.setState({ load_buscar:true })
    

    const res = await axios.post('os/pendentes', {
      loja: this.state.loja._id
    })
    const data = res.data
    await this.setState({ res: data,load_buscar:false })

  } catch (error) {
    this.setState({ load_buscar:false })
  }
  }

  // funcao pra selecionar a loja
  setLoja = item => {
    const novo = JSON.parse(JSON.stringify(item))
    this.setState({
      loja: novo
    })
  }
  // redireciona pra listagem de lojas
  gotoLojas = () => {
    this.props.navigation.navigate('select_loja', {
      lista: this.props.store.loja,
      atual: this.state.loja,
      onSelect: this.setLoja
    })
  }
  render () {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack()
              }}
            >
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Pendentes</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Separator bordered />
          <ListItem onPress={() => this.gotoLojas()}>
            <Body>
              <Text note>Empresa</Text>
              <Text>
                {this.state.loja.nome
                  ? this.state.loja.nome
                  : 'Selecione uma empresa'}
              </Text>
            </Body>
            <Right>
              <Icon name='arrow-forward' />
            </Right>
          </ListItem>

          <Separator bordered />

          {this.state.load_buscar ? (
            <Button disabled full onPress={() => {}}>
              <Text>Aguarde</Text>
            </Button>
          ) : (
            <Button
              full
              onPress={() => {
                if (this.state.loja._id) this.getPendentes()
                else alert('Selecione a Empresa')
              }}
            >
              <Text>Buscar</Text>
            </Button>
          )}

          <Separator bordered />

          {this.state.res.map((item, index) => {
            item.created_at_f = moment(item.created_at).format(
              'DD/MM/YY | HH:mm'
            )
            item.data_contabilidade_f = moment(item.data_contabilidade).format(
              'DD/MM'
            )
            return (
              <ListItem key={index} noIndent>
                <View style={{ width: 60 }}>
                  <Button
                    danger
                    onPress={() => {
                      this.cancelar(item._id)
                    }}
                  >
                    <Icon name='trash' />
                  </Button>
                </View>
                <Body>
                  <Text style={{ fontWeight: 'bold' }}>{item.loja.nome}</Text>
                  <Text style={{ fontWeight: 'bold' }}>
                    {item.bairro.nome} - {item.area.nome}
                  </Text>
                  <Text>
                    {item.area.km}-{' '}
                    {item.valor != item.area.valor
                      ? item.valor == 0
                        ? 'Garantia'
                        : 'R$ ' +
                          parseFloat(item.valor).toFixed(2) +
                          ' | Garantia '
                      : 'R$ ' + parseFloat(item.valor).toFixed(2)}
                  </Text>

                  <Text>{item.created_at_f}</Text>
                  <Text>
                    Status: {item.status} | OS: {item.os}
                  </Text>
                  <Text>
                    Turno: #{item.turno} - {item.data_contabilidade_f}
                  </Text>
                </Body>
              </ListItem>
            )
          })}
          <Separator bordered />
          <Button
            full
            danger
            onPress={() => {
              this.cancelarTodos()
            }}
          >
            <Text>Cancelar Todos</Text>
          </Button>
          <Separator bordered />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    store: state
  }
}

export default connect(mapStateToProps)(relatorios)
