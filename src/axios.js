import api from "./newaxios"
import { AsyncStorage } from 'react-native'
import { store } from './index'
import {  Toast } from 'native-base';
api.interceptors.request.use(async (config) => {
  const token = await AsyncStorage.getItem('token')
  if ( token != null ) {
   config.headers.Authorization = `Bearer ${token}`;
 }
  return config;
}, function (error) {
  return Promise.reject(error);
});

// api.interceptors.response.use(function (response) {
//   return response;
// }, function (error) {
//   console.log("PROBLEMA AXIOS",JSON.parse(JSON.stringify(error)))
//   if (!error.status){
//     Toast.show({
//       text: 'Sem conexão com o Servidor!',
//       buttonText: 'Ok',
//       duration: 6000,
//       position: "top"
//     })
//   }
//   store.dispatch({ type: 'STOP_LOAD' })
//   return Promise.reject(error);
// });

export default api

// cria instancia do axios fazendo com que ele use o token no headers
// da requisicao