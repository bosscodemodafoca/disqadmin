export default {
  
  header: '#ba222b',
  primary: '#007aff',
  success: '#5cb85c',
  danger: '#d9534f',
  warning: '#f0ad4e',
  info: '#62B1F6',

  bgsuccess: '#b3dbb3',
  bgdanger: '#d58683',
  bgwarning: '#eec080',

  darker: '#111',
  dark: '#333',
  regular: '#666',
  light: '#C0C0C0',
  lighter: '#eee',
  white: '#FFF',

  transparent: 'rgba(0, 0, 0, 0)',
}
