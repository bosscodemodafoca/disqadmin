import React from 'react'
import { createStackNavigator } from 'react-navigation'

// import { Button, Text, Icon, Footer, FooterTab } from "native-base";
import { Root } from 'native-base'
import login from './pages/login'
import menu from './pages/menu'
import motoboys from './pages/motoboys'
import motoboy from './pages/motoboy'
import lojas from './pages/lojas'
import loja from './pages/loja'
import bairro from './pages/bairro'
import setores from './pages/setores'
import setor from './pages/setor'
import turno from './pages/turno'
import select_motoboy from './pages/select_motoboy'
import select_motoboy2 from './pages/select_motoboy2'
import select_loja from './pages/select_loja'
import detalhes from './pages/detalhes'
import pendentes from './pages/pendentes'
import relatorios_pagamento from './pages/relatorios_pagamento'
import relatorios_recebimento from './pages/relatorios_recebimento'
import esqueci from './pages/esqueci'
import recuperar from './pages/recuperar'
import novasenha from './pages/novasenha'
import semconexao from './pages/semconexao'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './store'
export const store = createStore(rootReducer)

const RootNav = createStackNavigator(
  {
    menu,
    login,
    motoboys,
    motoboy,
    lojas,
    loja,
    setores,
    setor,
    relatorios_pagamento,
    relatorios_recebimento,
    select_motoboy,
    select_motoboy2,
    select_loja,
    detalhes,
    bairro,
    turno,
    pendentes,
    esqueci,
    recuperar,
    novasenha,
    semconexao
  },
  {
    initialRouteKey: 'menu',
    mode: 'modal',
    headerMode: 'none'
  }
)
const App = () => (
  <Root>
  <Provider store={store}>
    <RootNav />
  </Provider>
  </Root>
)
export default App
