import axios from "axios"
import { store } from './index'
import { Toast } from 'native-base';
import baseURL from './baseurl'
const api = axios.create({
  baseURL,
  timeout: 5000,
  headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
  },
})

api.interceptors.response.use(function (response) {
  return response;
}, function (error) {
console.log("PROBLEMA",JSON.parse(JSON.stringify(error)))

  if (!error.response){
    console.log(' entrou')
    Toast.show({
      text: 'Sem conexão com o Servidor! -',
      buttonText: 'Ok',
      duration: 6000,
      position: "top"
    })
  }
  store.dispatch({ type: 'STOP_LOAD' })
  return Promise.reject(error);
});

export default api

// pre configura o tipo de requisicao pra json e a url da api